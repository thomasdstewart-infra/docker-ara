FROM docker.io/debian:bullseye
LABEL name="docker-ara"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-ara"
LABEL maintainer="thomas@stewarts.org.uk"

ENV ARA_BASE_DIR=/opt/ara

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install build-essential gunicorn python3 python3-dev python3-pip python3-psycopg2 && \
    pip --no-cache-dir install ara[server] && \
    groupadd -r ara && useradd -r -g ara -d /opt/ara ara && \
    install -d -o ara -g ara -m 0750 /opt/ara && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /

EXPOSE 8000
USER ara:ara
CMD ["/entrypoint.sh"]