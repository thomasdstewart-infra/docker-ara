# ARA Docker Container on Debian

## Podman
* podman build -t ara --build-arg ARCH=amd64 .
* mkdir $(pwd)/ara
* echo "---" > $(pwd)/ara/settings.yaml
* uidgid=$(podman run ara getent passwd ara | awk -F: '{print $3 ":" $4}')
* podman unshare chown $uidgid -R $(pwd)/ara
* podman run -d -p 8000:8080 -v $(pwd)/ara:/opt/ara

